import './App.css';
import { Autocomplete } from './components/Autocomplete';
import AutocompleteHooks from './components/AutocompleteHooks';

function App() {
  return (
    <div className="App">
      <Autocomplete />
    </div>
  );
}

export default App;
