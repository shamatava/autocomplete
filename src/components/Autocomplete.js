import React from 'react';
import { AutoComplete } from 'antd';
import 'antd/dist/antd.css';
const { Option } = AutoComplete;

const API_URL = 'http://jsonplaceholder.typicode.com/users';

export class Autocomplete extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [],
            value: ''
        }
    }

    async componentDidMount() {
        await this.fetchJsonData()
    }

    /*
      This async function fetchs data from json placeholder api.
      It filteres data to return only items that contains search value. 
      Then this data is mapping and store in state to use it in Autocomplete option.
    */

    fetchJsonData = async () => {
        try {
            const response = await fetch(API_URL);
            const json = await response.json();
            let filteredData = json.filter(data => data.name.toLowerCase().indexOf(this.state.value.toLowerCase()) > -1);
            let items = filteredData.map(data => ({ value: data.name, label: data.name }))
            this.setState({ data: items })
        } catch (error) {
            console.log(error)
        }
    }

    /*
      This async function is finding new options accoring to the search value.
    */

    onChange = async (data) => {
        this.setState({ value: data })
        try {
            await this.fetchJsonData()
        } catch (error) {
            console.log(error)
        }
    }

    render() {

        /*
          In Ant design system can't customize option in direct way.
          So using this way
        */

        const options = this.state.data.map(item => {

            const index = this.state.value ? item.label.toLowerCase().indexOf(this.state.value.toLowerCase()) : -1 //If search value exist in word, find the index where search value starts in word.

            if (index > -1) { //if search value exist in data item

                let length = this.state.value.length;   // search word length
                let prefix = item.label.substring(0, index);  //start string from zero untill search word starts
                let suffix = item.label.substring(index + length); //part which starts after match word 
                let match = item.label.substring(index, index + length); //highlited match word


                return (
                    <Option key={item.value} value={item.value}>
                        {prefix}
                        <span style={{ color: 'red' }}>{match}</span>
                        {suffix}
                    </Option>
                )
            }

            return (
                <Option key={item.value} value={item.value}>
                    {item.value}
                </Option>
            )

        })

        return (
            <div className='ant-autocomplete-wrapper'>
                <h1>React Autocomplete Demo</h1>
                <AutoComplete
                    style={{ width: 250 }}
                    onChange={this.onChange}
                    placeholder="Enter your text"
                >
                    {options}
                </AutoComplete >
            </div>
        )
    }
}