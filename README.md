# How To Install

Download the source code using git or else download and unzip the zip file.

Open a terminal window and go to the project root folder.

You need to have npm installed globally.

Run npm i to install the required libraries.


# How To Run

Open a terminal window and make sure you are in the project root folder.

Run the following command for a dev server.

npm start

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Open the browser's Developer Tools window to see any errors in the Console.